import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import AmountField from '../AmountField';
import { decimalPad } from '../../helpers';

class Catalog extends Component {
  static propTypes = {
    products: PropTypes.array,
    onAddToOrder: PropTypes.func.isRequired
  };

  static defaultProps = {
    products: []
  };

  handleAdd(index, product) {
    const quantity = parseInt(
      ReactDOM.findDOMNode(this[`quantity_field_${index}`]).value,
      10
    );
    this.props.onAddToOrder(product, quantity);
  }

  render() {
    const { products } = this.props;
    return (
      <div>
        <h2>Product Catalogue</h2>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Description</th>
              <th className="align-right">Price</th>
              <th className="align-right">Amount</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product, index) => (
              <tr key={`product-${product.id}`}>
                <td>#{product.id}</td>
                <td>{product.description}</td>
                <td className="align-right">€{decimalPad(product.price)}</td>
                <td className="align-right">
                  <AmountField
                    className="align-right"
                    ref={ref => (this[`quantity_field_${index}`] = ref)}
                  />
                </td>
                <td>
                  <button
                    onClick={() => this.handleAdd.bind(this)(index, product)}
                  >
                    Add
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Catalog;
