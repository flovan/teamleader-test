import React from 'react';
import PropTypes from 'prop-types';
import { decimalPad } from '../../helpers';

const Order = ({ order, onBack, onMakeOrder, onRemoveFromOrder }) => (
  <div>
    <header className="space-children">
      <h1>Order #{order.id} detail</h1>
      <div>
        <button onClick={onBack}>Back to overview</button>
        {!!order.items.length && (
          <button onClick={() => onMakeOrder(order.id)}>Save order</button>
        )}
      </div>
    </header>
    {!!order.items.length && (
      <div>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Description</th>
              <th className="align-right">Amount</th>
              <th className="align-right">Price</th>
              <th className="align-right">Total</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            {order.items.map((product, index) => (
              <tr key={`product-${index}`}>
                <td>#{product.id}</td>
                <td>{product.description}</td>
                <td className="align-right">{product.quantity}</td>
                <td className="align-right">€{decimalPad(product.price)}</td>
                <td className="align-right">
                  <strong>€{decimalPad(product.total)}</strong>
                </td>
                <td>
                  <button onClick={() => onRemoveFromOrder(order.id, index)}>
                    Remove
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="align-right">
          <strong>Order total:</strong> €{decimalPad(order.total)}
        </div>
      </div>
    )}
    {!order.items.length && <p>This order is empty</p>}
  </div>
);

Order.propTypes = {
  order: PropTypes.object.isRequired,
  onBack: PropTypes.func.isRequired,
  onMakeOrder: PropTypes.func.isRequired,
  onRemoveFromOrder: PropTypes.func.isRequired
};
Order.defaultProps = {
  orders: []
};

export default Order;
